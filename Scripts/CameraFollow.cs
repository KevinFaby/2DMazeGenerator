﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform pj;

	// Use this for initialization
	void Start () {
		Screen.SetResolution (760, 1280, true);
	}

	// Update is called once per frame
	void Update()
	{
		this.transform.localPosition = new Vector3(pj.transform.localPosition.x, pj.transform.localPosition.y, -10);
	}
}
