﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {


	public Vector2 speed = new Vector2(5, 5);
	public Text TextScore;
	public int score;

	private Vector2 movement;
	private Rigidbody2D RB2D;


	// Use this for initialization
	void Start () {
		score = 0;
		TextScore.text = "Score : " + score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");

		movement = new Vector2(
			speed.x * inputX,
			speed.y * inputY);
		
	}

	void FixedUpdate()
	{
		if (RB2D == null)
			RB2D = GetComponent<Rigidbody2D> ();
		RB2D.velocity = movement;
	}

	public void UpgradeScore()
	{
		score++;
	}

	public int GetScore()
	{
		return score;
	}
}
