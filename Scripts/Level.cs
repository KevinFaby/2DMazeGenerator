﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

	public Player player;
	public GameObject exit;
	public GameObject wall_Vertical;
	public GameObject wall_Horizontal;

	private int height;
	private int width;
	private float xValue;
	private float yValue;
	private char[,] maze;
	private Vector2 ExitPos;
	string returnMap;

	// Use this for initialization
	void Start () {
		MakeMap ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("escape"))
			Application.Quit ();
		if (Input.GetKeyDown ("r"))
			MakeMap ();
	}

	public void MakeMap()
	{
		height = 9;
		width = 5;

		for (int i = 0; i < player.GetScore(); i++) {
			width++;
			if (i % 2 == 0)
				height++;
			else
				height += 2;
		}

		gameObject.transform.localScale	= new Vector2 (width, height);

		Destroy_wall ();
		Build_border ();
		buildMaze ();
		player.transform.localPosition = new Vector2 (width / -2 , Random.Range(height / 2, height / -2));
		exit.transform.localPosition = new Vector2 (width / 2, Random.Range(height / 2, height / -2));
		if (width % 2 == 0) {
			xValue = (float)player.transform.localPosition.x + 0.5f;
			Debug.Log (xValue);
			player.transform.localPosition = new Vector2 (xValue, (float)player.transform.localPosition.y);
			xValue = (float)exit.transform.localPosition.x - 0.5f;
			exit.transform.localPosition = new Vector2 (xValue, (float)exit.transform.localPosition.y);
		}
		if (height % 2 == 0) {
			yValue = (float)player.transform.localPosition.y - 0.5f;
			player.transform.localPosition = new Vector2 ((float)player.transform.localPosition.x, yValue);
			yValue = (float)exit.transform.localPosition.y - 0.5f;
			exit.transform.localPosition = new Vector2 ((float)exit.transform.localPosition.x, yValue);
		}

	}

	void Destroy_wall()
	{
		GameObject[] wd = GameObject.FindGameObjectsWithTag ("Wall");
		foreach (GameObject wall in wd)
			Destroy (wall);
	}

	void Build_border()
	{
		// Bordure Gauche
		for (int i = 0; i < height; i++) {
			var wh = Instantiate (wall_Vertical);
			wh.transform.localPosition = new Vector2 (width / -2, (height / 2) - i);
			if (width % 2 != 0) {
				xValue = (float)wh.transform.localPosition.x - 0.5f;
				wh.transform.localPosition = new Vector2 (xValue, (float)wh.transform.localPosition.y);
			}
			if (height % 2 == 0) {
				yValue = (float)wh.transform.localPosition.y - 0.5f;
				wh.transform.localPosition = new Vector2 ( (float)wh.transform.localPosition.x, yValue);
			}
		}

		// Bordure droite
		for (int i = 0; i < height; i++) {
			var wh = Instantiate (wall_Vertical);
			wh.transform.localPosition = new Vector2 (width / 2, (height / 2) - i);
			if (width % 2 != 0) {
				xValue = (float)wh.transform.localPosition.x + 0.5f;
				wh.transform.localPosition = new Vector2 (xValue, (float)wh.transform.localPosition.y);
			}
			if (height % 2 == 0) {
				yValue = (float)wh.transform.localPosition.y - 0.5f;
				wh.transform.localPosition = new Vector2 ( (float)wh.transform.localPosition.x, yValue);
			}
		}

		// Bordure haute
		for (int i = 0; i < width; i++) {
			var ww = Instantiate (wall_Horizontal);
			ww.transform.localPosition = new Vector2 ((width / 2) - i, (height / 2) + .5f);
			if (width % 2 == 0) {
				xValue = (float)ww.transform.localPosition.x - 0.5f;
				ww.transform.localPosition = new Vector2 (xValue, (float)ww.transform.localPosition.y);
			}
			if (height % 2 == 0) {
				yValue = (float)ww.transform.localPosition.y - 0.5f;
				ww.transform.localPosition = new Vector2 ( (float)ww.transform.localPosition.x, yValue);
			}
		}

		// Bordure basse
		for (int i = 0; i < width; i++) {
			var ww = Instantiate (wall_Horizontal);
			ww.transform.localPosition = new Vector2 ((width / 2) - i, (height / -2) - .5f);
			if (width % 2 == 0) {
				xValue = (float)ww.transform.localPosition.x - 0.5f;
				ww.transform.localPosition = new Vector2 (xValue, (float)ww.transform.localPosition.y);
			}
			if (height % 2 == 0) {
				yValue = (float)ww.transform.localPosition.y + 0.5f;
				ww.transform.localPosition = new Vector2 ( (float)ww.transform.localPosition.x, yValue);
			}
		}
	}

	void buildMaze()
	{
		MazeGenerator mg = new MazeGenerator ();
		mg.generate (height, width);
		maze = mg.getMaze ();
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				buildWall (maze[i, j],i, j);
		
		foreach (var map in maze)
			returnMap += map;
		Debug.Log (returnMap);
	}

	void buildWall(char id, int haut, int large)
	{
		switch (id) {
		case '1':
			var w11 = Instantiate (wall_Vertical); // mur droit
			var w12 = Instantiate (wall_Vertical); // mur gauche
			w11.name = "1 : droit";
			w12.name = "1 : gauche";

			if (width % 2 == 0) {
				w11.transform.localPosition = new Vector2 ((width / -2) + 1 + large, (height / 2) - haut);
				w12.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - haut);
			} else {
				w11.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) - haut);
				w12.transform.localPosition = new Vector2 ((width / -2) - .5f + large, (height / 2) - haut);
			}
			if (height % 2 == 0) {
				w11.transform.localPosition = new Vector2 (w11.transform.localPosition.x , w11.transform.localPosition.y - 0.5f);
				w12.transform.localPosition = new Vector2 (w12.transform.localPosition.x, w12.transform.localPosition.y - 0.5f);
			}
			if (large > 0 && maze [haut, large - 1] == '9')
				Destroy (w12);

			if (large < width - 1 && maze [haut, large + 1] == '7')
				Destroy(w11);

			break;
		case '2':
			var w21 = Instantiate (wall_Horizontal); // mur bas
			var w22 = Instantiate (wall_Horizontal); // mur haut
			w21.name = "2 : Bas";
			w22.name = "2 : Haut";

			if (width % 2 == 0) {
				w21.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) - .5f - haut);
				w22.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) + .5f - haut);
			} else {
				w21.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - .5f - haut);
				w22.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) + .5f - haut);
			}
			if (height % 2 == 0) {
				w21.transform.localPosition = new Vector2 (w21.transform.localPosition.x , w21.transform.localPosition.y - 0.5f);
				w22.transform.localPosition = new Vector2 (w22.transform.localPosition.x, w22.transform.localPosition.y - 0.5f);
			}

			if (haut > 0 && maze [haut - 1, large] == '8')
				Destroy(w22);
			if (haut < height - 1 && maze [haut + 1, large] == 'A')
				Destroy(w21);
			
			break;
		case '3':
			var w31 = Instantiate (wall_Horizontal); // mur haut
			var w32 = Instantiate (wall_Vertical); // mur gauche
			w31.name = "3 : haut";
			w32.name = "3 : gauche";

			if (width % 2 == 0) {
				w31.transform.localPosition = new Vector2 ((width / -2) +.5f + large, (height / 2) + .5f - haut);
				w32.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - haut);
			} else {
				w31.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) + .5f - haut);
				w32.transform.localPosition = new Vector2 ((width / -2) -.5f + large, (height / 2) - haut);
			}
			if (height % 2 == 0) {
				w31.transform.localPosition = new Vector2 (w31.transform.localPosition.x , w31.transform.localPosition.y - 0.5f);
				w32.transform.localPosition = new Vector2 (w32.transform.localPosition.x, w32.transform.localPosition.y - 0.5f);
			}

			if (haut > 0 && maze [haut - 1, large] == '8')
				Destroy(w31);
			if (large > 0 && maze [haut, large - 1] == '9')
				Destroy(w32);
			
			break;
		case '4':
			var w41 = Instantiate (wall_Horizontal); // mur haut
			var w42 = Instantiate (wall_Vertical); // mur droite
			w41.name = "4 : haut";
			w42.name = "4 : droite";

			if (width % 2 == 0) {
				w41.transform.localPosition = new Vector2 ((width / -2) +.5f + large, (height / 2) + .5f - haut);
				w42.transform.localPosition = new Vector2 ((width / -2) + 1 + large, (height / 2) - haut);
			} else {
				w41.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) + .5f - haut);
				w42.transform.localPosition = new Vector2 ((width / -2) +.5f + large, (height / 2) - haut);
			}
			if (height % 2 == 0) {
				w41.transform.localPosition = new Vector2 (w41.transform.localPosition.x , w41.transform.localPosition.y - 0.5f);
				w42.transform.localPosition = new Vector2 (w42.transform.localPosition.x, w42.transform.localPosition.y - 0.5f);
			}


			if (haut < height - 1 && maze [haut + 1, large] == 'A')
				Destroy(w41);
			if (large < width - 1 && maze [haut, large + 1] == '7')
				Destroy(w42);
			
			break;
		case '5':
			var w51 = Instantiate (wall_Horizontal); // mur bas
			var w52 = Instantiate (wall_Vertical); // mur droite
			w51.name = "5 : bas";
			w52.name = "5 : droite";

			if (width % 2 == 0) {
				w51.transform.localPosition = new Vector2 ((width / -2) +.5f + large, (height / 2) - .5f - haut);
				w52.transform.localPosition = new Vector2 ((width / -2) + 1 + large, (height / 2) - haut);
			} else {
				w51.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - .5f - haut);
				w52.transform.localPosition = new Vector2 ((width / -2) +.5f + large, (height / 2) - haut);
			}
			if (height % 2 == 0) {
				w51.transform.localPosition = new Vector2 (w51.transform.localPosition.x , w51.transform.localPosition.y - 0.5f);
				w52.transform.localPosition = new Vector2 (w52.transform.localPosition.x, w52.transform.localPosition.y - 0.5f);
			}

			if (haut > 0 && maze [haut - 1, large] == '8')
				Destroy(w51);
			if (large < width - 1 && maze [haut, large + 1] == '7')
				Destroy(w52);
			
			break;
		case '6':
			var w61 = Instantiate (wall_Horizontal); // mur bas
			var w62 = Instantiate (wall_Vertical); // mur gauche
			w61.name = "6 : bas";
			w62.name = "6 : gauche";

			if (width % 2 == 0) {
				w61.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) - .5f - haut);
				w62.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - haut);
			} else {
				w61.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - .5f - haut);
				w62.transform.localPosition = new Vector2 ((width / -2) - .5f + large, (height / 2) - haut);
			}
			if (height % 2 == 0) {
				w61.transform.localPosition = new Vector2 (w61.transform.localPosition.x , w61.transform.localPosition.y - 0.5f);
				w62.transform.localPosition = new Vector2 (w62.transform.localPosition.x, w62.transform.localPosition.y - 0.5f);
			}

			if (haut < height - 1 && maze [haut + 1, large] == 'A')
				Destroy(w61);
			if (large < width - 1 && maze [haut, large + 1] == '7')
				Destroy(w62);

			break;
		case '7':
			var w71 = Instantiate (wall_Vertical); // mur gauche
			w71.name = "7 : gauche";

			if (width % 2 == 0)
				w71.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - haut);
			else
				w71.transform.localPosition = new Vector2 ((width / -2) - .5f + large, (height / 2) - haut);
			if (height % 2 == 0)
				w71.transform.localPosition = new Vector2 (w71.transform.localPosition.x, w71.transform.localPosition.y - 0.5f);
			break;
		case '8':
			var w81 = Instantiate (wall_Horizontal); // mur haut
			w81.name = "8 : haut";

			if (width % 2 == 0)
				w81.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) + .5f - haut);
			else
				w81.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) + .5f - haut);
			if (height % 2 == 0)
				w81.transform.localPosition = new Vector2 (w81.transform.localPosition.x, w81.transform.localPosition.y - 0.5f);
			break;
		case '9':
			var w91 = Instantiate (wall_Vertical); // mur droit
			w91.name = "9 : droit";

			if (width % 2 == 0)
				w91.transform.localPosition = new Vector2 ((width / -2) + 1 + large, (height / 2) - haut);
			else
				w91.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) - haut);
			if (height % 2 == 0)
				w91.transform.localPosition = new Vector2 (w91.transform.localPosition.x, w91.transform.localPosition.y - 0.5f);
			break;
		case 'A':
			var wA1 = Instantiate (wall_Horizontal); // mur bas
			wA1.name = "A : bas";

			if (width % 2 == 0)
				wA1.transform.localPosition = new Vector2 ((width / -2) + .5f + large, (height / 2) - .5f - haut);
			else
				wA1.transform.localPosition = new Vector2 ((width / -2) + large, (height / 2) - .5f - haut);
			if (height % 2 == 0)
				wA1.transform.localPosition = new Vector2 (wA1.transform.localPosition.x, wA1.transform.localPosition.y - 0.5f);
			break;
		}
	}
}

