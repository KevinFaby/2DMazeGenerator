﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {

	char[,] maze;

	int height;
	int width;

	bool build = true;
	Vector2 pos = new Vector2(0, 0);
	List<Vector2> way = new List<Vector2>();
	int intWay = 0;
	int lastDir = 2;
	int dir;
	string returnMap;

	public void generate(int xValue, int yValue)
	{
		height = xValue;
		width = yValue;

		maze = new char[height, width];
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				maze[i, j] = '0';

		while (build)
		{
			printMap ();
			way.Add (pos);

			check_way ();
			putID ();

			while (dir == 0) {
				way.RemoveAt (intWay);
				intWay--;

				if (intWay <= -1) {
					dir = 1;
					build = false;
				} else 
					pos = way [intWay];
				
				if (dir == 0)
					check_way ();

				if (dir != 0) {
					if ((dir == 2 && maze [(int)pos.x, (int)pos.y] == '1') || (dir == 3 && maze [(int)pos.x, (int)pos.y] == '3') || (dir == 4 && maze [(int)pos.x, (int)pos.y] == '6'))
						maze [(int)pos.x, (int)pos.y] = '7';
					if ((dir == 4 && maze [(int)pos.x, (int)pos.y] == '2') || (dir == 1 && maze [(int)pos.x, (int)pos.y] == '3') || (dir == 2 && maze [(int)pos.x, (int)pos.y] == '4'))
						maze [(int)pos.x, (int)pos.y] = '8';
					if ((dir == 1 && maze [(int)pos.x, (int)pos.y] == '1') || (dir == 3 && maze [(int)pos.x, (int)pos.y] == '4') || (dir == 4 && maze [(int)pos.x, (int)pos.y] == '5'))
						maze [(int)pos.x, (int)pos.y] = '9';
					if ((dir == 3 && maze [(int)pos.x, (int)pos.y] == '2') || (dir == 2 && maze [(int)pos.x, (int)pos.y] == '5') || (dir == 1 && maze [(int)pos.x, (int)pos.y] == '6'))
						maze [(int)pos.x, (int)pos.y] = 'A';
					if ((dir == 1 && maze [(int)pos.x, (int)pos.y] == '7') || (dir == 2 && maze [(int)pos.x, (int)pos.y] == '9') || (dir == 3 && maze [(int)pos.x, (int)pos.y] == '8') || (dir == 4 && maze [(int)pos.x, (int)pos.y] == 'A'))
						maze [(int)pos.x, (int)pos.y] = 'B';
				}
			}

			switch (dir) {
			case 1:
				pos.y--;
				break;
			case 2:
				pos.y++;
				break;
			case 3:
				pos.x--;
				break;
			case 4:
				pos.x++;
				break;
			}

			lastDir = dir;
			intWay++;
		}
	}

	void check_way()
	{
		List<int> choice = new List<int> ();
		int i = 0;

		if ((int) pos.y > 0 && maze [(int)pos.x, ((int)pos.y) - 1] == '0') {
			choice.Add (1);
			i++;
		}
		if ((int) pos.y < width - 1 && maze [(int)pos.x, ((int)pos.y) + 1] == '0') {
			choice.Add (2);
			i++;
		}
		if ((int) pos.x > 0 && maze [((int)pos.x) - 1, (int)pos.y] == '0') {
			choice.Add (3);
			i++;
		}
		if ((int) pos.x < height - 1 && maze [((int)pos.x) + 1, (int)pos.y] == '0') {
			choice.Add (4);
			i++;
		}

		if (i == 0)
			dir = 0;
		else
			dir = choice [Random.Range (0, i)];
	}

	void putID()
	{
		var save = dir;
		if (dir == 0)
			dir = lastDir;
		if ((lastDir == 3 && dir == 3) || (lastDir == 4 && dir == 4)) {
			if ((int) pos.x == 0)
				maze [(int)pos.x, (int)pos.y] = '9';
			else if ((int)pos.x == width - 1)
				maze [(int)pos.x, (int)pos.y] = '7';
			else
				maze [(int)pos.x, (int)pos.y] = '1';
		}
		if ((lastDir == 1 && dir == 1) || (lastDir == 2 && dir == 2))
			maze [(int)pos.x, (int)pos.y] = '2';
		if ((lastDir == 1 && dir == 4) || (lastDir == 3 && dir == 2))
			maze [(int)pos.x, (int)pos.y] = '3';
		if ((lastDir == 2 && dir == 4) || (lastDir == 3 && dir == 1))
			maze [(int)pos.x, (int)pos.y] = '4';
		if ((lastDir == 2 && dir == 3) || (lastDir == 4 && dir == 1))
			maze [(int)pos.x, (int)pos.y] = '5';
		if ((lastDir == 1 && dir == 3) || (lastDir == 4 && dir == 2))
			maze [(int)pos.x, (int)pos.y] = '6';

		dir = save;
	}
		
	public char[,] getMaze()
	{
		return maze;
	}

	void printMap()
	{
		char save = maze [(int)pos.x, (int)pos.y];
		maze [(int)pos.x, (int)pos.y] = 'x';
		foreach (var map in maze)
			returnMap += map;
		Debug.Log (returnMap);
		maze [(int)pos.x, (int)pos.y] = save;
	}
}
