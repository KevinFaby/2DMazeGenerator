﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit : MonoBehaviour {

	public Level level;
	public Player player;
	public Text TextScore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
		player.UpgradeScore ();
		TextScore.text = "Score : " + player.GetScore ().ToString ();
		coll.gameObject.transform.position = new Vector2 (0, 0);
		level.MakeMap ();
	}
}
